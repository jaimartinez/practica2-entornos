﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    public class Class1
    {
        public static void abecedario()
        {
            for (int i = 'A'; i <= 'Z'; i++)
            {
                for (int j = 'A'; j <= 'Z'; j++)
                {
                    for (int k = 'A'; k <= 'Z'; k++)
                    {
                        System.Console.WriteLine((char)i);
                        System.Console.WriteLine((char)j);
                        System.Console.WriteLine((char)k);
                    }
                }
            }


        }


        public static Boolean esDivisor(int dividendo, int divisor)
        {
            Boolean resultado = false;

            if (dividendo % divisor == 0)
            {
                resultado = true;
            }

            return resultado;
        }


        public static double calcularPrecioFinal(double precioBase,
            Boolean ivaAplicado, double porcentajeIva)
        {

            if (ivaAplicado)
            {
                return precioBase * porcentajeIva + precioBase;
            }

            return precioBase;

        }



        public static String invertirCadena(String cadena)
        {
            String cadenaInversa = "";

            for (int i = 0; i < cadena.Length; i++)
            {
                cadenaInversa = cadena[i] + cadenaInversa;
            }

            return cadenaInversa;
        }


        public static Boolean esEntero(String cadena)
        {

            for (int i = 0; i < cadena.Length; i++)
            {

                if (cadena[0] == '-' && cadena.Length == 1)
                {
                    return false;
                }

                if (i == 0 && cadena[i] != '-' && cadena[i] < '0' && cadena[i] > '9')
                {
                    return false;
                }

                if ((cadena[i] < '0' || cadena[i] > '9') && i != 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
