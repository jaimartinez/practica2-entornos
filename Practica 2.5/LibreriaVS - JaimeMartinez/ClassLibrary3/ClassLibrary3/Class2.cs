﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    public class Class2
    {
        public static int redondearAlza(double numeroLeido)
        {

            if (numeroLeido != (int)numeroLeido)
            {
                return (int)numeroLeido + 1;
            }
            else
            {

                return (int)numeroLeido;

            }

        }


        public static int redondearBaja(double numeroLeido)
        {

            return (int)numeroLeido;
        }


        public static int potencia(int numeroBase, int exponente)
        {
            int resultado = 1;

            for (int i = 0; i < exponente; i++)
            {
                resultado = resultado * numeroBase;
            }
            return resultado;
        }


        public static int maximo(int numero1, int numero2)
        {
            if (numero1 > numero2)
            {
                return numero1;
            }
            else
            {

                return numero2;

            }
        }


        public static int minimo(int numeroA, int numeroB)
        {
            if (numeroA > numeroB)
            {
                return numeroB;
            }
            else
            {

                return numeroA;

            }

        }
    }
}
