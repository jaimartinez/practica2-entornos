﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
           
            ClassLibrary3.Class1.calcularPrecioFinal(20, true, 8.36);
            ClassLibrary3.Class1.esDivisor(256, 5);
            ClassLibrary3.Class1.esEntero("3556");
            ClassLibrary3.Class1.invertirCadena("Hola me llamo Juan");
            ClassLibrary3.Class1.abecedario();

            ClassLibrary3.Class2.maximo(321, 6512);
            ClassLibrary3.Class2.minimo(654, 28);
            ClassLibrary3.Class2.potencia(256, 10);
            ClassLibrary3.Class2.redondearAlza(2.59);
            ClassLibrary3.Class2.redondearBaja(8.49);

            System.Console.ReadKey();

        }
    }
}
