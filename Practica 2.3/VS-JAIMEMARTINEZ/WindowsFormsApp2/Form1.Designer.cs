﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.usuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.películasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.másVistasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novedadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.géneroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infantilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comediaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.másVistasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.novedadesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.géneroToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.infantilToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comediaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.acciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dramaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.terrorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.romanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(628, 469);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Descargar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(190, 473);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(118, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Acepto los términos";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 369);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Edad:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Sexo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Volumen:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(245, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Buscar:";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(339, 254);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(234, 45);
            this.trackBar1.TabIndex = 9;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(109, 369);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(32, 20);
            this.numericUpDown1.TabIndex = 10;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuarioToolStripMenuItem,
            this.películasToolStripMenuItem,
            this.seriesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(797, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // usuarioToolStripMenuItem
            // 
            this.usuarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perfilToolStripMenuItem,
            this.configuraciónToolStripMenuItem,
            this.ayudaToolStripMenuItem,
            this.cambiarUsuarioToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.usuarioToolStripMenuItem.Name = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.usuarioToolStripMenuItem.Text = "Usuario";
            // 
            // películasToolStripMenuItem
            // 
            this.películasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.másVistasToolStripMenuItem,
            this.novedadesToolStripMenuItem,
            this.géneroToolStripMenuItem});
            this.películasToolStripMenuItem.Name = "películasToolStripMenuItem";
            this.películasToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.películasToolStripMenuItem.Text = "Películas";
            // 
            // seriesToolStripMenuItem
            // 
            this.seriesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.másVistasToolStripMenuItem1,
            this.novedadesToolStripMenuItem1,
            this.géneroToolStripMenuItem1});
            this.seriesToolStripMenuItem.Name = "seriesToolStripMenuItem";
            this.seriesToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.seriesToolStripMenuItem.Text = "Series";
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // cambiarUsuarioToolStripMenuItem
            // 
            this.cambiarUsuarioToolStripMenuItem.Name = "cambiarUsuarioToolStripMenuItem";
            this.cambiarUsuarioToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.cambiarUsuarioToolStripMenuItem.Text = "Cambiar usuario";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // másVistasToolStripMenuItem
            // 
            this.másVistasToolStripMenuItem.Name = "másVistasToolStripMenuItem";
            this.másVistasToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.másVistasToolStripMenuItem.Text = "Más vistas";
            // 
            // novedadesToolStripMenuItem
            // 
            this.novedadesToolStripMenuItem.Name = "novedadesToolStripMenuItem";
            this.novedadesToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.novedadesToolStripMenuItem.Text = "Novedades";
            // 
            // géneroToolStripMenuItem
            // 
            this.géneroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infantilToolStripMenuItem,
            this.comediaToolStripMenuItem,
            this.acciónToolStripMenuItem,
            this.dramaToolStripMenuItem,
            this.terrorToolStripMenuItem,
            this.romanceToolStripMenuItem});
            this.géneroToolStripMenuItem.Name = "géneroToolStripMenuItem";
            this.géneroToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.géneroToolStripMenuItem.Text = "Género";
            // 
            // infantilToolStripMenuItem
            // 
            this.infantilToolStripMenuItem.Name = "infantilToolStripMenuItem";
            this.infantilToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.infantilToolStripMenuItem.Text = "Infantil";
            // 
            // comediaToolStripMenuItem
            // 
            this.comediaToolStripMenuItem.Name = "comediaToolStripMenuItem";
            this.comediaToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.comediaToolStripMenuItem.Text = "Comedia";
            // 
            // acciónToolStripMenuItem
            // 
            this.acciónToolStripMenuItem.Name = "acciónToolStripMenuItem";
            this.acciónToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.acciónToolStripMenuItem.Text = "Acción";
            // 
            // dramaToolStripMenuItem
            // 
            this.dramaToolStripMenuItem.Name = "dramaToolStripMenuItem";
            this.dramaToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.dramaToolStripMenuItem.Text = "Drama";
            // 
            // terrorToolStripMenuItem
            // 
            this.terrorToolStripMenuItem.Name = "terrorToolStripMenuItem";
            this.terrorToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.terrorToolStripMenuItem.Text = "Terror";
            // 
            // romanceToolStripMenuItem
            // 
            this.romanceToolStripMenuItem.Name = "romanceToolStripMenuItem";
            this.romanceToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.romanceToolStripMenuItem.Text = "Romance";
            // 
            // másVistasToolStripMenuItem1
            // 
            this.másVistasToolStripMenuItem1.Name = "másVistasToolStripMenuItem1";
            this.másVistasToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.másVistasToolStripMenuItem1.Text = "Más vistas";
            // 
            // novedadesToolStripMenuItem1
            // 
            this.novedadesToolStripMenuItem1.Name = "novedadesToolStripMenuItem1";
            this.novedadesToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.novedadesToolStripMenuItem1.Text = "Novedades";
            // 
            // géneroToolStripMenuItem1
            // 
            this.géneroToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infantilToolStripMenuItem1,
            this.comediaToolStripMenuItem1,
            this.acciónToolStripMenuItem1,
            this.dramaToolStripMenuItem1,
            this.terrorToolStripMenuItem1,
            this.romanceToolStripMenuItem1});
            this.géneroToolStripMenuItem1.Name = "géneroToolStripMenuItem1";
            this.géneroToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.géneroToolStripMenuItem1.Text = "Género";
            // 
            // infantilToolStripMenuItem1
            // 
            this.infantilToolStripMenuItem1.Name = "infantilToolStripMenuItem1";
            this.infantilToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.infantilToolStripMenuItem1.Text = "Infantil";
            // 
            // comediaToolStripMenuItem1
            // 
            this.comediaToolStripMenuItem1.Name = "comediaToolStripMenuItem1";
            this.comediaToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.comediaToolStripMenuItem1.Text = "Comedia";
            // 
            // acciónToolStripMenuItem1
            // 
            this.acciónToolStripMenuItem1.Name = "acciónToolStripMenuItem1";
            this.acciónToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.acciónToolStripMenuItem1.Text = "Acción";
            // 
            // dramaToolStripMenuItem1
            // 
            this.dramaToolStripMenuItem1.Name = "dramaToolStripMenuItem1";
            this.dramaToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.dramaToolStripMenuItem1.Text = "Drama";
            // 
            // terrorToolStripMenuItem1
            // 
            this.terrorToolStripMenuItem1.Name = "terrorToolStripMenuItem1";
            this.terrorToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.terrorToolStripMenuItem1.Text = "Terror";
            // 
            // romanceToolStripMenuItem1
            // 
            this.romanceToolStripMenuItem1.Name = "romanceToolStripMenuItem1";
            this.romanceToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.romanceToolStripMenuItem1.Text = "Romance";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(797, 25);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(109, 323);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(62, 17);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Hombre";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(221, 323);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(51, 17);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Mujer";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // button3
            // 
            this.button3.Image = global::WindowsFormsApp2.Properties.Resources.siguiente_pista;
            this.button3.Location = new System.Drawing.Point(554, 186);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 23);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Image = global::WindowsFormsApp2.Properties.Resources.playpause;
            this.button2.Location = new System.Drawing.Point(373, 186);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 23);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Image = global::WindowsFormsApp2.Properties.Resources.anterior_pista;
            this.button1.Location = new System.Drawing.Point(195, 186);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 23);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(339, 115);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(251, 20);
            this.textBox1.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(797, 519);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "CINEZONE";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem películasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem másVistasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novedadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem géneroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infantilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comediaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem másVistasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem novedadesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem géneroToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem infantilToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem comediaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem acciónToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dramaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem terrorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem romanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox textBox1;
    }
}

