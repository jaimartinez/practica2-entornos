﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            do
            {

                System.Console.WriteLine("************************");
                System.Console.WriteLine("1- Método 1");
                System.Console.WriteLine("2- Metodo 2");
                System.Console.WriteLine("3- Metodo 3");
                System.Console.WriteLine("4- Metodo 4");
                System.Console.WriteLine("5- Salir");
                System.Console.WriteLine("************************");

                System.Console.WriteLine("Introduce una opcion");
                String opcionLeida = System.Console.ReadLine();
                opcion = int.Parse(opcionLeida);
               

                switch (opcion)
                {
                    case 1:
                        parImpar();
                        break;
                    case 2:
                        esEntero();
                        break;
                    case 3:
                        System.Console.WriteLine(palabraMasLarga());
                        break;
                    case 4:
                        System.Console.WriteLine(invertirCadena());
                        break;
                    case 5:
                        System.Console.WriteLine("Has salido del programa");
                        break;
                    default:
                        System.Console.WriteLine("La opción que has seleccionado es incorrecta");
                        break;
                }

            } while (opcion != 5);
        }
        public static void parImpar()
        {

            System.Console.WriteLine("Introduce un numero");
            String numeroLeido = System.Console.ReadLine();
            int numero = int.Parse(numeroLeido);

            if (numero % 2 == 0)
            {
                System.Console.WriteLine("El numero es par");
            }
            else
            {
                System.Console.WriteLine("El numero es impar");
            }

        }

        public static void esEntero()
        {

            System.Console.WriteLine("Introduce un numero entero");
            String cadena = System.Console.ReadLine();

            for (int i = 0; i < cadena.Length; i++)
            {

                if (cadena[0] == '-' && cadena.Length == 1)
                {
                    System.Console.WriteLine("El numero no es entero");
                }

                if (i == 0 && cadena[i] != '-' && cadena[i] < '0' && cadena[i] > '9')
                {
                    System.Console.WriteLine("El numero no es entero");
                }

                if ((cadena[i] < '0' || cadena[i] > '9') && i != 0)
                {
                    System.Console.WriteLine("El numero no es entero");
                }
            }

            System.Console.WriteLine("El numero es entero");
        }



        static String palabraMasLarga()
        {

            System.Console.WriteLine("Introduce palabras separadas por un espacio");
            String cadena = System.Console.ReadLine();

            String palabraEncontrada;
            int inicioPalabra = 0;
            String palabraMayor = "";

            cadena = cadena + " ";

            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] == ' ')
                {
                    palabraEncontrada = cadena.Substring(inicioPalabra, i + 1);
                    if (palabraMayor.Length < palabraEncontrada.Length)
                    {
                        palabraMayor = palabraEncontrada;
                    }
                    inicioPalabra = i + 1;
                }
            }

            return palabraMayor;
        }


        static String invertirCadena()
        {

            System.Console.WriteLine("Introduce una cadena");
            String cadena = System.Console.ReadLine();

            String cadenaInversa = "";
            //asdf
            for (int i = 0; i < cadena.Length; i++)
            {
                cadenaInversa = cadena[i] + cadenaInversa;
            }

            return cadenaInversa;
        }
    }
}
