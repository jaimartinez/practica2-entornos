package libreriaJava;

import java.util.Scanner;

public class LibreriaJava1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


	}
	
	public static void abecedario(){
		for(int i = 'A'; i <= 'Z'; i++ ){
			for(int j = 'A'; j<= 'Z' ; j++){
				for(int k = 'A'; k <= 'Z'; k++){
					System.out.print((char)i);
					System.out.print((char)j);
					System.out.println((char)k);
				}
			}
		}
		
		
	}
	
	
	public static int aleatorio(int fin){
		return (int) Math.round((Math.random() * fin));
	}
	
	
	public static String palabraMasLarga(String cadena) {
		String palabraEncontrada;
		int inicioPalabra = 0;
		String palabraMayor = "";
		
		cadena = cadena + " ";
			
		for(int i = 0; i < cadena.length(); i++){
			if(cadena.charAt(i) == ' '){
				palabraEncontrada = cadena.substring(inicioPalabra, i+1);
				if(palabraMayor.length() < palabraEncontrada.length()){
					palabraMayor = palabraEncontrada;
				}
				inicioPalabra = i+1;
			}
		}
		
		return palabraMayor;
	}
	
	
	 public static String invertirCadena(String string) {
			String cadenaInversa="";
		
			for(int i = 0; i < string.length(); i++){		
				cadenaInversa = string.charAt(i) + cadenaInversa;
			}

			return cadenaInversa;
		}
	 
	 
	 public static boolean esEntero(String cadena) {
	
			for(int i = 0; i< cadena.length(); i++){
			
				if(cadena.charAt(0) == '-' && cadena.length() == 1){
					return false;
				}
				
				if(i == 0 && cadena.charAt(i) != '-' && cadena.charAt(i) < '0' && cadena.charAt(i) > '9'){
					return false;
				}
				
				if((cadena.charAt(i) < '0' || cadena.charAt(i) > '9') && i != 0){
					return false;
				}
			}
			
			return true;
		}


}
