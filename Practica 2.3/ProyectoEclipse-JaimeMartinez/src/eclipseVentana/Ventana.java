package eclipseVentana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SpinnerNumberModel;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/iconos/simbolo-de-reproductor-de-video.png")));
		setTitle("CINEZONE");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Usuario");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmPerfil = new JMenuItem("Perfil");
		mnNewMenu.add(mntmPerfil);
		
		JMenuItem mntmConfiguracin = new JMenuItem("Configuraci\u00F3n");
		mnNewMenu.add(mntmConfiguracin);
		
		JMenuItem mntmAyuda = new JMenuItem("Ayuda");
		mnNewMenu.add(mntmAyuda);
		
		JMenuItem mntmCambiarUsuario = new JMenuItem("Cambiar usuario");
		mnNewMenu.add(mntmCambiarUsuario);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnNewMenu.add(mntmSalir);
		
		JMenu mnNewMenu_1 = new JMenu("Pel\u00EDculas");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmMsVistas = new JMenuItem("M\u00E1s vistas");
		mnNewMenu_1.add(mntmMsVistas);
		
		JMenuItem mntmNovedades = new JMenuItem("Novedades");
		mnNewMenu_1.add(mntmNovedades);
		
		JMenu mnNewMenu_3 = new JMenu("G\u00E9nero");
		mnNewMenu_1.add(mnNewMenu_3);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Infantil");
		mnNewMenu_3.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Comedia");
		mnNewMenu_3.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Acci\u00F3n");
		mnNewMenu_3.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Drama");
		mnNewMenu_3.add(mntmNewMenuItem_3);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Terror");
		mnNewMenu_3.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Romance");
		mnNewMenu_3.add(mntmNewMenuItem_5);
		
		JMenu mnNewMenu_2 = new JMenu("Series");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("M\u00E1s vistas");
		mnNewMenu_2.add(mntmNewMenuItem_6);
		
		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Novedades");
		mnNewMenu_2.add(mntmNewMenuItem_7);
		
		JMenu mnNewMenu_4 = new JMenu("G\u00E9nero");
		mnNewMenu_2.add(mnNewMenu_4);
		
		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Infantil");
		mnNewMenu_4.add(mntmNewMenuItem_8);
		
		JMenuItem mntmNewMenuItem_9 = new JMenuItem("Comedia");
		mnNewMenu_4.add(mntmNewMenuItem_9);
		
		JMenuItem mntmNewMenuItem_10 = new JMenuItem("Acci\u00F3n");
		mnNewMenu_4.add(mntmNewMenuItem_10);
		
		JMenuItem mntmNewMenuItem_11 = new JMenuItem("Drama");
		mnNewMenu_4.add(mntmNewMenuItem_11);
		
		JMenuItem mntmNewMenuItem_12 = new JMenuItem("Terror");
		mnNewMenu_4.add(mntmNewMenuItem_12);
		
		JMenuItem mntmNewMenuItem_13 = new JMenuItem("Romance");
		mnNewMenu_4.add(mntmNewMenuItem_13);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 434, 23);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/icon.png")));
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/iconcxfx.png")));
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/boton-actualizar.png")));
		toolBar.add(btnNewButton_2);
		
		JLabel lblBuscar = new JLabel("Buscar: ");
		lblBuscar.setBounds(46, 38, 73, 14);
		contentPane.add(lblBuscar);
		
		textField = new JTextField();
		textField.setBounds(129, 35, 203, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/anterior-pista.png")));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_3.setBounds(46, 79, 89, 23);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/playpause.png")));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4.setBounds(161, 79, 89, 23);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(Ventana.class.getResource("/iconos/siguiente-pista.png")));
		btnNewButton_5.setBounds(279, 79, 89, 23);
		contentPane.add(btnNewButton_5);
		
		JLabel lblVolumen = new JLabel("Volumen: ");
		lblVolumen.setBounds(58, 130, 77, 14);
		contentPane.add(lblVolumen);
		
		JSlider slider = new JSlider();
		slider.setBackground(new Color(255, 255, 153));
		slider.setBounds(132, 118, 200, 26);
		contentPane.add(slider);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(24, 166, 46, 14);
		contentPane.add(lblSexo);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setBounds(24, 194, 46, 14);
		contentPane.add(lblEdad);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		spinner.setBounds(76, 191, 29, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setBackground(new Color(255, 255, 153));
		rdbtnHombre.setBounds(59, 162, 80, 23);
		contentPane.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBackground(new Color(255, 255, 153));
		rdbtnMujer.setBounds(141, 162, 109, 23);
		contentPane.add(rdbtnMujer);
		
		JCheckBox chckbxAceptoLosTrminos = new JCheckBox("Acepto los t\u00E9rminos");
		chckbxAceptoLosTrminos.setBackground(new Color(255, 255, 153));
		chckbxAceptoLosTrminos.setBounds(121, 210, 157, 23);
		contentPane.add(chckbxAceptoLosTrminos);
		
		JButton btnDescargar = new JButton("Descargar");
		btnDescargar.setBounds(302, 210, 122, 23);
		contentPane.add(btnDescargar);
	}
}
