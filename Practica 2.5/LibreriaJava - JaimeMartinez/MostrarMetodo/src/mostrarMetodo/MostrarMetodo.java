package mostrarMetodo;

import libreriaJava.LibreriaJava1;
import libreriaJava.LibreriaJava2;

public class MostrarMetodo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LibreriaJava1.abecedario();
		System.out.println(LibreriaJava1.aleatorio(100));
		System.out.println(LibreriaJava1.palabraMasLarga("Hola me llamo Juan"));
		System.out.println(LibreriaJava1.invertirCadena("Hola me llamo Juan"));
		System.out.println(LibreriaJava1.esEntero("2485"));
	
		System.out.println(LibreriaJava2.redondearAlza(2.56));
		System.out.println(LibreriaJava2.redondearBaja(876.3));
		System.out.println(LibreriaJava2.potencia(10, 8));
		System.out.println(LibreriaJava2.maximo(289, 300));
		System.out.println(LibreriaJava2.minimo(102, 400));
		
	}

}
